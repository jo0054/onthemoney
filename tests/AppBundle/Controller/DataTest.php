<?php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class DataTest extends WebTestCase
{
    //test search response
    public function testSearchResponse()
    {
        $client = static::createClient();

        $client->request('GET', '/search/kt19');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    //test latlon response
    public function testLatLonResponse()
    {
        $client = static::createClient();

        $client->request('GET', '/lat/51.365900/lon/-0.278930/radius/0.1');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    //test valid json search
    public function testValidJSONSearch()
    {
        $client = static::createClient();

        $client->request('GET', '/search/kt19');

        $this->assertJson($client->getResponse()->getContent());
    }

    //test valid json search
    public function testValidJSONLatLon()
    {
        $client = static::createClient();

        $client->request('GET', '/lat/51.365900/lon/-0.278930/radius/0.1');

        $this->assertJson($client->getResponse()->getContent());
    }

    //test search result is valid postcode
    public function testValidSearchPostCodes()
    {
        $client = static::createClient();

        $client->request('GET', '/search/kt19');

        $results = json_decode($client->getResponse()->getContent());

        $regex = '/([A-PR-UWYZ]([A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y])?|[0-9]([0-9]|[A-HJKPSTUW])?) ?[0-9][ABD-HJLNP-UW-Z]{2})/m';

        foreach ($results as $result) {
            $this->assertRegExp($regex, $result->postCode);
        }
    }

    //test search result is valid postcode
    public function testValidLatonLonPostCodes()
    {
        $client = static::createClient();

        $client->request('GET', '/lat/51.365900/lon/-0.278930/radius/0.1');

        $results = json_decode($client->getResponse()->getContent());

        $regex = '/([A-PR-UWYZ]([A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y])?|[0-9]([0-9]|[A-HJKPSTUW])?) ?[0-9][ABD-HJLNP-UW-Z]{2})/m';

        foreach ($results as $result) {
            $this->assertRegExp($regex, $result->postCode);
        }
    }
}