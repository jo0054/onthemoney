<?php

namespace AppBundle\Command;

use AppBundle\Entity\PostCode;
use League\Csv\Reader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;

use Symfony\Component\HttpKernel\Kernel;
use ZipArchive;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Finder\Finder;
use League\Csv;

class PostCodeCommand extends ContainerAwareCommand
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();

        $this->em = $em;
    }

    protected function configure() {
        $this->setName('postCode')
            ->setDescription('Downloads and imports UK postCodes into database')
            ->setHelp('This command downloads and imports UK postCodes into database');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $downloadUrl = 'https://parlvid.mysociety.org/os/code-point/codepo_gb-2020-05.zip';

        $downloadPath = $this->getContainer()->get('kernel')->getProjectDir().'/data';

        //Disable temporary SSL
        $arrContextOptions = array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            )
        );

        //Create ProgressBar
        $progress = new ProgressBar($output);

        //Create Stream Context with Callback to update ProgressBar
        $context = stream_context_create($arrContextOptions, array('notification' => function ($notification_code, $severity, $message, $message_code, $bytes_transferred, $bytes_max) use ($output, $progress) {
            switch ($notification_code) {
                case STREAM_NOTIFY_RESOLVE:
                case STREAM_NOTIFY_AUTH_REQUIRED:
                case STREAM_NOTIFY_COMPLETED:
                case STREAM_NOTIFY_FAILURE:
                case STREAM_NOTIFY_AUTH_RESULT:
                    /** Ignore That */
                    break;

                case STREAM_NOTIFY_REDIRECTED:
                    $output->writeln("Redirect To : ", $message);
                    break;

                case STREAM_NOTIFY_CONNECT:
                    $output->writeln("Connected...");
                    break;
                case STREAM_NOTIFY_FILE_SIZE_IS:
                    /** @var $progress ProgressBar */
                    $output->writeln("Downloading...");
                    $progress->start($bytes_max);
                    break;
                case STREAM_NOTIFY_PROGRESS:
                    $progress->setProgress($bytes_transferred);
                    break;
            }
        }));

        //Download file
        $streamContent = file_get_contents($downloadUrl,false,$context);

        $progress->finish();

        $output->writeln("");
        $output->writeln("Download Complete...");

        file_put_contents($downloadPath.'/postCodeData.zip', $streamContent);

        //Unzip file
        $zip = new ZipArchive;
        if ($zip->open($downloadPath.'/postCodeData.zip') === TRUE) {
            $zip->extractTo($downloadPath . '/postCodeData');
            $zip->close();
            $output->writeln("File unzipped");
        } else {
            $output->writeln("Unzip failed");
        }

        //Loop through each CSV file
        $finder = new Finder();
        // find all files in the current directory
        $finder->files()->in($downloadPath . '/postCodeData/Data/CSV/');


        // check if there are any search results
        if ($finder->hasResults()) {

            foreach ($finder as $file) {
                $output->writeln("Processing: ". $file->getRelativePathname());

                //Parse CSV
                $reader = Reader::createFromPath($downloadPath . '/postCodeData/Data/CSV/' . $file->getRelativePathname());
                $data = $reader->fetchAll();

                gc_enable();
                $this->em->getConnection()->getConfiguration()->setSQLLogger(null);

                foreach ($data as $row) {
                    //create new postcode
                    $postCode = (new PostCode())
                        ->setPostCode(str_replace(" ", "", $row[0]))
                        ->setPQ($row[1])
                        ->setEastings($row[2])
                        ->setNorthings($row[3])
                        ->setCountryCode($row[4])
                        ->setRegionalCode($row[5])
                        ->setHaCode($row[6])
                        ->setDistrictCode($row[7])
                        ->setCountyCode($row[8])
                        ->setWardCode($row[9]);

                    $this->em->persist($postCode);

                    $output->writeln(array_values($row)[0] . " Processed");
                }

                $output->writeln("Persisting...");
                $this->em->flush();
                $this->em->clear();
                gc_collect_cycles();
                $output->writeln($file->getRelativePathname() . " Post Codes Added");

            }
        }
    }
}