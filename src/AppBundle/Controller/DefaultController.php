<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PostCode;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use ZipArchive;
use League\Csv\Reader;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use PHPCoord;
use PHPCoord\LatLng;
use PHPCoord\RefEll;

class DefaultController extends Controller
{
    /**
     * @Route("/search/{postcode}", name="search")
     */
    public function searchAction(EntityManagerInterface $em, $postcode) {

        if(is_string($postcode)) {

            //Search DB
            $repository = $em->getRepository('AppBundle:PostCode');
            $query = $repository->createQueryBuilder('p')
                ->select('p.postCode')
                ->where('p.postCode LIKE :postcode')
                ->setParameter('postcode', '%'.trim($postcode).'%')
                ->getQuery();
            $results = $query->getResult();
        }

        //Format JSON response
        $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);

        $response = new Response($serializer->serialize($results, 'json'));

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/lat/{lat}/lon/{lon}/radius/{rad}", name="latlon")
     */
    public function latLonAction(EntityManagerInterface $em, $lat, $lon, $rad) {
        if(is_numeric($rad) && is_numeric($lat) && is_numeric($lon)) {
            $maxLat = $lat + rad2deg($rad/6371);
            $minLat = $lat - rad2deg($rad/6371);
            $maxLon = $lon + rad2deg(asin($rad/6371) / cos(deg2rad($lat)));
            $minLon = $lon - rad2deg(asin($rad/6371) / cos(deg2rad($lat)));

            //convert to OSRef
            $minLatLon = new LatLng($minLat, $minLon, 0, RefEll::wgs84());
            $maxLatLon = new LatLng($maxLat, $maxLon, 0, RefEll::wgs84());

            $minOSRef = $minLatLon->toOSRef();
            $maxOSRef = $maxLatLon->toOSRef();

            $minEast = $minOSRef->getX();
            $maxEast = $maxOSRef->getX();

            $minNorth = $minOSRef->getY();
            $maxNorth = $maxOSRef->getY();

            //Search DB
            $repository = $em->getRepository('AppBundle:PostCode');
            $query = $repository->createQueryBuilder('p')
                ->select('p.postCode')
                ->where('p.eastings BETWEEN :minEast AND :maxEast')
                ->andWhere('p.northings BETWEEN  :minNorth AND :maxNorth')
                ->setParameter('minEast', $minEast)
                ->setParameter('maxEast', $maxEast)
                ->setParameter('minNorth', $minNorth)
                ->setParameter('maxNorth', $maxNorth)
                ->getQuery();
            $results = $query->getResult();
        } else {
            //return error
        }

        //Format JSON response
        $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);

        $response = new Response($serializer->serialize($results, 'json'));

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/loop", name="loop")
     */
    public function test() {
        $downloadPath = $this->get('kernel')->getProjectDir().'/data';

        //Loop through each CSV file
        $finder = new Finder();
        // find all files in the current directory
        $finder->files()->in($downloadPath . '/postCodeData/Data/CSV/');

        //$serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);

        // check if there are any search results
        if ($finder->hasResults()) {
            foreach ($finder as $file) {
                echo "<pre>";
                echo $downloadPath . '/postCodeData/Data/CSV/' . $file->getRelativePathname() . "<br />";
                echo "</pre>";
                $reader = Reader::createFromPath($downloadPath . '/postCodeData/Data/CSV/' . $file->getRelativePathname());
                //Import files
                //$data = $serializer->decode(file_get_contents($downloadPath . '/postCodeData/Data/CSV/' . $file->getRelativePathname()), 'csv');
                $data = $reader->fetchAll();

                foreach ($data as $row) {
                    echo "<pre>";
                    print_r(($row));
                    echo "</pre>";
                }

            }
        }
    }
}
