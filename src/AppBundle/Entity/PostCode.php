<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="postcode")
 */
class PostCode
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $postCode;

    /**
     * @ORM\Column(type="integer")
     */
    private $pQ;

    /**
     * @ORM\Column(type="integer")
     */
    private $eastings;

    /**
     * @ORM\Column(type="integer")
     */
    private $northings;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $countryCode;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $regionalCode;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $haCode;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $countyCode;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $districtCode;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $wardCode;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set postCode
     *
     * @param string $postCode
     *
     * @return PostCode
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;

        return $this;
    }

    /**
     * Get postCode
     *
     * @return string
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * Set pQ
     *
     * @param integer $pQ
     *
     * @return PostCode
     */
    public function setPQ($pQ)
    {
        $this->pQ = $pQ;

        return $this;
    }

    /**
     * Get pQ
     *
     * @return integer
     */
    public function getPQ()
    {
        return $this->pQ;
    }

    /**
     * Set eastings
     *
     * @param integer $eastings
     *
     * @return PostCode
     */
    public function setEastings($eastings)
    {
        $this->eastings = $eastings;

        return $this;
    }

    /**
     * Get eastings
     *
     * @return integer
     */
    public function getEastings()
    {
        return $this->eastings;
    }

    /**
     * Set northings
     *
     * @param integer $northings
     *
     * @return PostCode
     */
    public function setNorthings($northings)
    {
        $this->northings = $northings;

        return $this;
    }

    /**
     * Get northings
     *
     * @return integer
     */
    public function getNorthings()
    {
        return $this->northings;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     *
     * @return PostCode
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set regionalCode
     *
     * @param string $regionalCode
     *
     * @return PostCode
     */
    public function setRegionalCode($regionalCode)
    {
        $this->regionalCode = $regionalCode;

        return $this;
    }

    /**
     * Get regionalCode
     *
     * @return string
     */
    public function getRegionalCode()
    {
        return $this->regionalCode;
    }

    /**
     * Set haCode
     *
     * @param string $haCode
     *
     * @return PostCode
     */
    public function setHaCode($haCode)
    {
        $this->haCode = $haCode;

        return $this;
    }

    /**
     * Get haCode
     *
     * @return string
     */
    public function getHaCode()
    {
        return $this->haCode;
    }

    /**
     * Set countyCode
     *
     * @param string $countyCode
     *
     * @return PostCode
     */
    public function setCountyCode($countyCode)
    {
        $this->countyCode = $countyCode;

        return $this;
    }

    /**
     * Get countyCode
     *
     * @return string
     */
    public function getCountyCode()
    {
        return $this->countyCode;
    }

    /**
     * Set districtCode
     *
     * @param string $districtCode
     *
     * @return PostCode
     */
    public function setDistrictCode($districtCode)
    {
        $this->districtCode = $districtCode;

        return $this;
    }

    /**
     * Get districtCode
     *
     * @return string
     */
    public function getDistrictCode()
    {
        return $this->districtCode;
    }

    /**
     * Set wardCode
     *
     * @param string $wardCode
     *
     * @return PostCode
     */
    public function setWardCode($wardCode)
    {
        $this->wardCode = $wardCode;

        return $this;
    }

    /**
     * Get wardCode
     *
     * @return string
     */
    public function getWardCode()
    {
        return $this->wardCode;
    }
}
