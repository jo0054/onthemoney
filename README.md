OnTheMoney Post Code Import & Search Test
=========================================

Pre Requisites
--------------
The default database is a MySQL databse on localhost named:<br />
otm

This can be changed in the config

<strong>Run the following commands to create database and create schema:</strong>

<code>php bin/console doctrine:database:drop --force</code><br>
<code>php bin/console doctrine:database:create</code><br>
<code>php bin/console doctrine:schema:update --force</code>

How to use
--------------
<strong>To run the command to download & import postcode:</strong><br/><br/>
<code>php bin/console postcode</code>

<strong>To start built in server</strong> (If not using XAMP/ WAMP etc)<br/><br/>
<code>php bin/console server:run</code>

<strong>To search for a post code enter the following URL in a browser or make GET call<br/><br/>
http://127.0.0.1:8000/search/{postcode}

<strong>To make a latitude/longitude search enter the following URL in a browser or make GET call (radius is in km)<br/><br/>
http://127.0.0.1:8000/lat/{latitude}/lon/{longtitude}/radius/{radius}

<strong>To run unit tests:</strong><br />
<code>php ./vendor/bin/simple-phpunit</code>

Notes
------
[1]: I found composer often used ran out of memory during installs when using PHP X86, If you run into that issue please increase PHP memory<br />
[2]: This can also happen during the Doctrine flushes, once again increase memory (I found 512M worked best)<br />
[3]: I had to manually install phpunit-bridge & css browserkit because of composer memory issues

Enhancements
-------------
Below are the following enhancements, I would have made if time permitted
 * Create a batch file for set up command functions
 * Used a crawler to grab file (in case the link changed)
 * Check the zip and CSV file integrity (in case of corrupt download)
 * Clean up code structure to comply with PSR-4
 * Include a duplicates check for import
 * Use inserts instead of doctrine for import for speed
 * Have the console command as its own service so that ContainerAware is not used for COmmand
 * Remove spaces from post codes for uniformity
 * Add authetication token & IP whitelist to GET calls for security
 * Secure the data folder or move to a non public location
 * Cache the database calls or postcodes to speed search
 * Enchance latitude, longitude search by using advanced bounding circle calculation and storing latitude and longitude in database
 * Add data fixtures and mock repos to test cases and inject factory service to test data in database 